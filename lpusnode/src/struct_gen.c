/* Library for ECSS Package Utilization Standard (PUS)
 *
 * Copyright (C) 2020 TU Darmstadt Space Technology e.V.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * Generate pre-defined PUS structure for a given subservice
 */
#include <err.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "lpus.h"

/** Converted PUS structure to string */
void struct_to_str(uint16_t sub, lpus_struct_t lpus_struct, char* str)
{
    uint8_t* p = NULL;
    tm03xx_param_t* pa = NULL;

    str[0] = '\0';

    switch (sub) {
        case TM0101_SUCCESS_ACCEPT:
            p = lpus_struct.st01.tm0101.tc_req_id;
            sprintf(str, "TM0101 to [%x.%x.%x.%x]", p[0], p[1], p[2], p[3]);
            break;

        case TM0102_FAILED_ACCEPT:
            p = lpus_struct.st01.tm0102.tc_req_id;
            sprintf(str, "TM0102 to [%x.%x.%x.%x], fail 0x%x %.*s", p[0], p[1],
                    p[2], p[3], lpus_struct.st01.tm0102.fail.code,
                    (int)lpus_struct.st01.tm0102.fail.data_size,
                    (char*)lpus_struct.st01.tm0102.fail.data);
            break;

        case TM0103_SUCCESS_START:
            p = lpus_struct.st01.tm0103.tc_req_id;
            sprintf(str, "TM0103 to [%x.%x.%x.%x]", p[0], p[1], p[2], p[3]);
            break;

        case TM0104_FAILED_START:
            p = lpus_struct.st01.tm0104.tc_req_id;
            sprintf(str, "TM0104 to [%x.%x.%x.%x], fail 0x%x %.*s", p[0], p[1],
                    p[2], p[3], lpus_struct.st01.tm0104.fail.code,
                    (int)lpus_struct.st01.tm0104.fail.data_size,
                    (char*)lpus_struct.st01.tm0104.fail.data);
            break;

        case TM0105_SUCCESS_PROGRESS:
            p = lpus_struct.st01.tm0105.tc_req_id;
            sprintf(str, "TM0105 to [%x.%x.%x.%x], step %d", p[0], p[1], p[2],
                    p[3], lpus_struct.st01.tm0105.step);
            break;

        case TM0106_FAILED_PROGRESS:
            p = lpus_struct.st01.tm0106.tc_req_id;
            sprintf(str, "TM0106 to [%x.%x.%x.%x], step %d, fail 0x%x %.*s",
                    p[0], p[1], p[2], p[3], lpus_struct.st01.tm0106.step,
                    lpus_struct.st01.tm0106.fail.code,
                    (int)lpus_struct.st01.tm0106.fail.data_size,
                    (char*)lpus_struct.st01.tm0106.fail.data);
            break;

        case TM0107_SUCCESS_COMPLETE:
            p = lpus_struct.st01.tm0107.tc_req_id;
            sprintf(str, "TM0107 to [%x.%x.%x.%x]", p[0], p[1], p[2], p[3]);
            break;

        case TM0108_FAILED_COMPLETE:
            p = lpus_struct.st01.tm0108.tc_req_id;
            sprintf(str, "TM0108 to [%x.%x.%x.%x], fail 0x%x %.*s", p[0], p[1],
                    p[2], p[3], lpus_struct.st01.tm0108.fail.code,
                    (int)lpus_struct.st01.tm0108.fail.data_size,
                    (char*)lpus_struct.st01.tm0108.fail.data);
            break;

        case TM0325_HOUSE_PARAM:
            pa = lpus_struct.st03.tm0325.params;
            sprintf(str, "TM0325 %x params[%d]: { %lu, %lu, %lu, %lu, %lu, %lu, %lu }",
                    lpus_struct.st03.tm0325.struct_id,
                    lpus_struct.st03.tm0325.params_num,
                    (long)pa[0].val, (long)pa[1].val, (long)pa[2].val,
                    (long)pa[3].val, (long)pa[4].val, (long)pa[5].val,
                    (long)pa[6].val);
            break;

        case TM0501_EV_INFO:
            sprintf(str, "TM0501 %x %.*s", lpus_struct.st05.tm0501.ev_id,
                    (int)lpus_struct.st05.tm0501.data_size,
                    (char*)lpus_struct.st05.tm0501.data);
            break;

        case TM0502_EV_LOW:
            sprintf(str, "TM0502 %x %.*s", lpus_struct.st05.tm0502.ev_id,
                    (int)lpus_struct.st05.tm0502.data_size,
                    (char*)lpus_struct.st05.tm0502.data);
            break;

        case TM0503_EV_MEDIUM:
            sprintf(str, "TM0503 %x %.*s", lpus_struct.st05.tm0503.ev_id,
                    (int)lpus_struct.st05.tm0503.data_size,
                    (char*)lpus_struct.st05.tm0503.data);
            break;

        case TM0504_EV_HIGH:
            sprintf(str, "TM0504 %x %.*s", lpus_struct.st05.tm0504.ev_id,
                    (int)lpus_struct.st05.tm0504.data_size,
                    (char*)lpus_struct.st05.tm0504.data);
            break;

        case TC0505_EN_REP:
            sprintf(str, "TC0505 ack: %x ev_list[%d]: { %x, %x, %x,... }",
                    lpus_struct.st05.tc0505.tc_ack.value,
                    lpus_struct.st05.tc0505.num,
                    lpus_struct.st05.tc0505.ev_list[0],
                    lpus_struct.st05.tc0505.ev_list[1],
                    lpus_struct.st05.tc0505.ev_list[2]);
            break;

        case TC0506_DIS_REP:
            sprintf(str, "TC0506 ack: %x ev_list[%d]: { %x, %x, %x,... }",
                    lpus_struct.st05.tc0506.tc_ack.value,
                    lpus_struct.st05.tc0506.num,
                    lpus_struct.st05.tc0506.ev_list[0],
                    lpus_struct.st05.tc0506.ev_list[1],
                    lpus_struct.st05.tc0506.ev_list[2]);
            break;

        case TC0507_REQ_DIS_LIST:
            sprintf(str, "TC0507 ack: %x", lpus_struct.st05.tc0507.tc_ack.value);
            break;

        case TM0508_RESP_DIS_LIST:
            sprintf(str, "TM0508 (n/a ack: %x) ev_list[%d]: { %x, %x, %x,... }",
                    lpus_struct.st05.tm0508.tc_ack.value,
                    lpus_struct.st05.tm0508.num,
                    lpus_struct.st05.tm0508.ev_list[0],
                    lpus_struct.st05.tm0508.ev_list[1],
                    lpus_struct.st05.tm0508.ev_list[2]);
            break;

        case TC0801_FUNC:
            p = lpus_struct.st08.tc0801.args;
            sprintf(str, "TC0801 ack: %x, f: %s, arg_n: %d { %x, %x, %x,... }",
                    lpus_struct.st08.tc0801.tc_ack.value,
                    lpus_struct.st08.tc0801.func_id,
                    lpus_struct.st08.tc0801.args_num, p[0], p[1], p[2]);
            break;

        default:
            break;
    }
}

/** Generate pre-defined PUS structure for a given subservice */
lpus_struct_t struct_gen(uint16_t sub)
{
    lpus_struct_t lpus_struct = { };
    static event_id_t ev_list[16] = {
        0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08,
        0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F, 0x10
    };
    static uint8_t tc_req_id[4] = { 0xFA, 0xFF, 0xD1, 0x48 };
    static uint8_t func_args[6] = { 0x45, 0x67, 0x89, 0xAB, 0xCD, 0xEF };

    switch (sub) {
        case TM0101_SUCCESS_ACCEPT:
            lpus_struct.st01.tm0101.tc_req_id = tc_req_id;
            break;

        case TM0102_FAILED_ACCEPT:
            lpus_struct.st01.tm0102.tc_req_id = tc_req_id;
            lpus_struct.st01.tm0102.fail.code = 0x5A;
            lpus_struct.st01.tm0102.fail.data = "oops";
            lpus_struct.st01.tm0102.fail.data_size = 4;
            break;

        case TM0103_SUCCESS_START:
            lpus_struct.st01.tm0103.tc_req_id = tc_req_id;
            break;

        case TM0104_FAILED_START:
            lpus_struct.st01.tm0104.tc_req_id = tc_req_id;
            lpus_struct.st01.tm0104.fail.code = 0xABCD;
            lpus_struct.st01.tm0104.fail.data = "happens";
            lpus_struct.st01.tm0104.fail.data_size = 7;
            break;

        case TM0105_SUCCESS_PROGRESS:
            lpus_struct.st01.tm0105.tc_req_id = tc_req_id;
            lpus_struct.st01.tm0105.step = 33;
            break;

        case TM0106_FAILED_PROGRESS:
            lpus_struct.st01.tm0106.tc_req_id = tc_req_id;
            lpus_struct.st01.tm0106.fail.code = 0x1234;
            lpus_struct.st01.tm0106.fail.data = "oh";
            lpus_struct.st01.tm0106.fail.data_size = 2;
            lpus_struct.st01.tm0105.step = 100;
            break;

        case TM0107_SUCCESS_COMPLETE:
            lpus_struct.st01.tm0107.tc_req_id = tc_req_id;
            break;

        case TM0108_FAILED_COMPLETE:
            lpus_struct.st01.tm0108.tc_req_id = tc_req_id;
            lpus_struct.st01.tm0108.fail.code = 0;
            lpus_struct.st01.tm0108.fail.data = NULL;
            lpus_struct.st01.tm0108.fail.data_size = 0;
            break;

        case TM0325_HOUSE_PARAM:
            lpus_struct.st03.tm0325.struct_id = 0xABCD;
            lpus_struct.st03.tm0325.params_num = 7;
            for (uint8_t i = 0; i < 7; i++) {
                lpus_struct.st03.tm0325.params[i].id = i;
                lpus_struct.st03.tm0325.params[i].size = sizeof(uint64_t);
                /* random value 0 - 160, where 160 is hight of temperature
                 * canvas in `web-gauge` UI
                 */
                lpus_struct.st03.tm0325.params[i].val = rand() % 160;
            }
            break;

        case TM0501_EV_INFO:
            lpus_struct.st05.tm0501.ev_id = 0x11;
            lpus_struct.st05.tm0501.data = "info";
            lpus_struct.st05.tm0501.data_size = 4;
            break;

        case TM0502_EV_LOW:
            lpus_struct.st05.tm0502.ev_id = 0x22;
            lpus_struct.st05.tm0502.data = "low";
            lpus_struct.st05.tm0502.data_size = 3;
            break;

        case TM0503_EV_MEDIUM:
            lpus_struct.st05.tm0503.ev_id = 0x33;
            lpus_struct.st05.tm0503.data = "medium";
            lpus_struct.st05.tm0503.data_size = 6;
            break;

        case TM0504_EV_HIGH:
            lpus_struct.st05.tm0504.ev_id = 0x44;
            lpus_struct.st05.tm0504.data = "high";
            lpus_struct.st05.tm0504.data_size = 4;
            break;

        case TC0505_EN_REP:
            lpus_struct.st05.tc0505.tc_ack.flags.accept_request = 1;
            lpus_struct.st05.tc0505.num = 8;
            lpus_struct.st05.tc0505.ev_list = ev_list;
            break;

        case TC0506_DIS_REP:
            lpus_struct.st05.tc0506.tc_ack.flags.accept_request = 0;
            lpus_struct.st05.tc0506.num = 11;
            lpus_struct.st05.tc0506.ev_list = ev_list;
            break;

        case TC0507_REQ_DIS_LIST:
            lpus_struct.st05.tc0507.tc_ack.flags.accept_request = 1;
            break;

        case TM0508_RESP_DIS_LIST:
            lpus_struct.st05.tm0508.num = 16;
            lpus_struct.st05.tm0508.ev_list = ev_list;
            break;

        case TC0801_FUNC:
            lpus_struct.st08.tc0801.tc_ack.flags.start_exec = 1;
            lpus_struct.st08.tc0801.tc_ack.flags.progress_exec = 1;
            lpus_struct.st08.tc0801.tc_ack.flags.complete_exec = 1;
            strcpy(lpus_struct.st08.tc0801.func_id, "kickflip");
            lpus_struct.st08.tc0801.args_num = 4;
            lpus_struct.st08.tc0801.args = func_args;
            lpus_struct.st08.tc0801.args_size = sizeof(func_args);
            break;

        default:
            break;
    }

    return lpus_struct;
}
