/*
 * Copyright (c) 2018 Artem Sheludko
 * Copyright (c) 2019 Tobias Marciszko
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

class Cube {

constructor(elemIdCanvas) {
    this.canvas  = document.getElementById(elemIdCanvas);
    this.context = this.canvas.getContext("2d");

    this.fill = false;

    this.angleX = 2 * Math.PI / 360;
    this.angleY = 2 * Math.PI / 720;
    this.angleZ = 2 * Math.PI / 1480;

    this.rotX = true;
    this.rotY = true;
    this.rotZ = false;

    this.cube;

    this.rotateAndDrawCube(0, 0, 0);
}

rotateAndDrawCube(digreeX, digreeY, digreeZ) {
    this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);

    this.cube = this.createCube(0, 0, 4, 2);

    this.angleX = digreeX * (2 * Math.PI / 360);
    this.angleY = digreeY * (2 * Math.PI / 720);
    this.angleZ = digreeZ * (2 * Math.PI / 1480);

    this.cube = this.rotateCubeX(this.cube);
    this.cube = this.rotateCubeY(this.cube);
    this.cube = this.rotateCubeZ(this.cube);

    this.drawCube(this.cube, 0);
}

// position: x: 0, y: 0, z: 6
// size 2 (length of one side)
createCube(x, y, z, size) {
    var start = {"x": x, "y": y, "z": z, "size": size};

    var bl = {x: x-size/2, y:y-size/2, z:z-size/2}
    var br = {x: x+size/2, y:y-size/2, z:z-size/2}
    var tl = {x:x-size/2, y:y+size/2, z:z-size/2}
    var tr = {x:x+size/2, y:y+size/2, z:z-size/2}

    var blz = {x:x-size/2, y:y-size/2, z:z+size/2}
    var brz = {x:x+size/2, y:y-size/2, z:z+size/2}
    var tlz = {x:x-size/2, y:y+size/2, z:z+size/2}
    var trz = {x:x+size/2, y:y+size/2, z:z+size/2}

    return {
        "bl": bl, 
        "br": br, 
        "tl": tl, 
        "tr": tr, 
        "blz": blz,
        "brz": brz,
        "tlz": tlz, 
        "trz": trz,
        "start": start
        };
}

worldToScreen(p, offset) {
    let factor = this.canvas.width;
    let screen = {
        x: p.x / p.z * factor + (this.canvas.width/2), 
        y: p.y / p.z * factor + (this.canvas.height / 2)
        };

    return screen
}

rotateCubeZ(cube) {
    let bl = this.rotateZ(cube.bl, cube.start);
    let br = this.rotateZ(cube.br, cube.start);
    let tl = this.rotateZ(cube.tl, cube.start);
    let tr = this.rotateZ(cube.tr, cube.start);
    let blz = this.rotateZ(cube.blz, cube.start);
    let brz = this.rotateZ(cube.brz, cube.start);
    let tlz = this.rotateZ(cube.tlz, cube.start);
    let trz = this.rotateZ(cube.trz, cube.start);
    return {
        "bl": bl, 
        "br": br, 
        "tl": tl, 
        "tr": tr, 
        "blz": blz,
        "brz": brz,
        "tlz": tlz, 
        "trz": trz,
        "start": cube.start
        }; 
}

rotateCubeX(cube) {
    let bl = this.rotateX(cube.bl, cube.start);
    let br = this.rotateX(cube.br, cube.start);
    let tl = this.rotateX(cube.tl, cube.start);
    let tr = this.rotateX(cube.tr, cube.start);
    let blz = this.rotateX(cube.blz, cube.start);
    let brz = this.rotateX(cube.brz, cube.start);
    let tlz = this.rotateX(cube.tlz, cube.start);
    let trz = this.rotateX(cube.trz, cube.start);
    return {
        "bl": bl, 
        "br": br, 
        "tl": tl, 
        "tr": tr, 
        "blz": blz,
        "brz": brz,
        "tlz": tlz, 
        "trz": trz,
        "start": cube.start
        }; 
}

rotateCubeY(cube) {
    let bl = this.rotateY(cube.bl, cube.start);
    let br = this.rotateY(cube.br, cube.start);
    let tl = this.rotateY(cube.tl, cube.start);
    let tr = this.rotateY(cube.tr, cube.start);
    let blz = this.rotateY(cube.blz, cube.start);
    let brz = this.rotateY(cube.brz, cube.start);
    let tlz = this.rotateY(cube.tlz, cube.start);
    let trz = this.rotateY(cube.trz, cube.start);
    return {
        "bl": bl, 
        "br": br, 
        "tl": tl, 
        "tr": tr, 
        "blz": blz,
        "brz": brz,
        "tlz": tlz, 
        "trz": trz,
        "start": cube.start
        }; 
}

translateY(cube, value) {

    var bl  = cube.bl; 
    var br  = cube.br; 
    var tl  = cube.tl; 
    var tr  = cube.tr; 
    var blz = cube.blz;
    var brz = cube.brz;
    var tlz = cube.tlz;
    var trz = cube.trz;
    var start = cube.start;

    bl.y  = cube.bl.y+value;
    br.y  = cube.br.y+value;
    tl.y  = cube.tl.y+value;
    tr.y  = cube.tr.y+value;
    blz.y = cube.blz.y+value;
    brz.y = cube.brz.y+value;
    tlz.y = cube.tlz.y+value;
    trz.y = cube.trz.y+value;
    start.y = cube.start.y-value;

    return {
        "bl":  bl, 
        "br":  br, 
        "tl":  tl, 
        "tr":  tr, 
        "blz": blz,
        "brz": brz,
        "tlz": tlz, 
        "trz": trz,
        "start": cube.start
        };
}

rotateZ(p, start) {
    let x = p.x - start.x;
    let y = p.y - start.y;
    return {x: x*Math.cos(this.angleZ) - y*Math.sin(this.angleZ) + start.x, y:y*Math.cos(this.angleZ) + x*Math.sin(this.angleZ) + start.y, z: p.z}
}

rotateX(p, start) {
    let z = p.z - start.z;
    let y = p.y - start.y;

    let yy = y*Math.cos(this.angleX) - z*Math.sin(this.angleX) + start.y
    let zz = (y*Math.sin(this.angleX) + z*Math.cos(this.angleX)) + start.z
    return {x: p.x, y: yy, z: zz}
}

rotateY(p, start) {
    let z = p.z - start.z;
    let x = p.x - start.x;

    let xx = x*Math.cos(this.angleY) + z*Math.sin(this.angleY) + start.x
    let zz = (-x*Math.sin(this.angleY) + z*Math.cos(this.angleY)) + start.z
    return {x: xx, y: p.y, z: zz}
}

drawLine(p0, p1) {
    this.context.beginPath();
    this.context.moveTo(p0.x, p0.y);
    this.context.lineTo(p1.x, p1.y);
    this.context.lineWidth = "2"
    this.context.strokeStyle = "#222";
    this.context.stroke();
}

/* Animation is unused - we draw by given angles instead
update() {
    context.clearRect(0, 0, canvas.width, canvas.height);
   
    // Cube
    if (rotZ === true) {
        cube = rotateCubeZ(cube);
    }

    if (rotY === true) {
        cube = rotateCubeY(cube);
    }

    if (rotX === true) {
        cube = rotateCubeX(cube);
    }

    drawCube(cube, 0);
    requestAnimationFrame(update);
   }
*/

drawCube(cube, offset) {
    let blr = this.worldToScreen(cube.bl, offset)
    let brr = this.worldToScreen(cube.br, offset)
    let tlr = this.worldToScreen(cube.tl, offset)
    let trr = this.worldToScreen(cube.tr, offset)
    let blrz = this.worldToScreen(cube.blz, offset)
    let brrz = this.worldToScreen(cube.brz, offset)
    let tlrz = this.worldToScreen(cube.tlz, offset)
    let trrz = this.worldToScreen(cube.trz, offset)

    let front = {p1: blr, p2: brr, p3: trr, p4: tlr, color: "#bb2222",};
    let back = {p1: blrz, p2: brrz, p3: trrz, p4: tlrz, color: "#22bb22",};
    let left = {p1: blr, p2: tlr, p3: tlrz, p4: blrz, color: "#2222bb"};  
    let right = {p1: brr, p2: trr, p3: trrz, p4: brrz, color: "#22bbbb"};
    let top = {p1: tlr, p2: trr, p3: trrz, p4: tlrz, color: "#bb22bb"};
    let bottom = {p1: blr, p2: brr, p3: brrz, p4: blrz, color: "#bbbb22"};

    var faces = [front, back, left, right, top, bottom];
    faces.forEach(this.drawFace, this);
}

drawFace(face) {

    let p1 = face.p1;
    let p2 = face.p2;
    let p3 = face.p3;
    let p4 = face.p4;

    let ctx = this.context;

    ctx.beginPath();
    ctx.moveTo(p1.x, p1.y);
    ctx.lineTo(p2.x, p2.y);
    ctx.lineTo(p3.x, p3.y);
    ctx.lineTo(p4.x, p4.y);
    ctx.closePath();
    ctx.strokeStyle = "white";
    ctx.stroke();
}

} /* end of class Cube */
