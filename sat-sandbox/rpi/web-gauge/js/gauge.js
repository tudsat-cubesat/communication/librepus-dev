var cube;
var tgraph;
var timer;

function show_xyz(x, y, z)
{
    var xyz = $('#xyz')

    xyz.html('<b>X:</b> ' + x + ' ' +
             '<b>Y:</b> ' + y + ' ' +
             '<b>Z:</b> ' + z + ' digree');
}

function show_temp(tempsArray)
{
    var temp = $('#temp');
    var i;

    var str = '<b>Temperatures:</b> ';
    for (i in tempsArray) {
        str += tempsArray[i] + ', ';
    }
    str += 'digree C';

    temp.html(str);
}

function gauge_init()
{
    /* disable files caching during periodic requests */
    $.ajaxSetup({cache: false});

    show_xyz(0, 0, 0);
    show_temp(new Array(0, 0, 0, 0));

    cube = new Cube('xyz_canvas');
    tgraph = new Temperature('temp_canvas');

    timer = setInterval(periodic_foo, 500);
}

function periodic_foo()
{
    $.get( "user-data/tm.txt", function(data) {
        // parse data as a regular expression
        var rexp = /TM0325.*{ ([0-9]+), ([0-9]+), ([0-9]+), ([0-9]+), ([0-9]+), ([0-9]+), ([0-9]+) }/;
        var match = data.match(rexp);

        var x = match[1];
        var y = match[2];
        var z = match[3];
        var tempsArray = new Array(match[4], match[5], match[6], match[7]);

        show_xyz(x, y, z);
        show_temp(tempsArray);

        cube.rotateAndDrawCube(x, y, z);
        tgraph.redrawTempGraph(tempsArray);
    });
}

$(document).ready(gauge_init);
