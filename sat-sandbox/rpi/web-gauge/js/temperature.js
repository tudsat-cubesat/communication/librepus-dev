class Temperature
{
    constructor(elemIdCanvas)
    {
        this.canvas  = document.getElementById(elemIdCanvas);
        this.context = this.canvas.getContext("2d");

        this.redrawTempGraph(new Array(0, 0));
    }

    redrawTempGraph(tempsArray)
    {
        this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);

        this.drawGraph(tempsArray);
    }

    drawGraph(tempsArray)
    {
        if (tempsArray.length < 2)
            return;

        let ctx = this.context;
        let x = 0;
        let deltaX = this.canvas.width / (tempsArray.length - 1);
        let y = 0;
        let i = 0;

        for (i in tempsArray) {
            y = this.canvas.height - tempsArray[i];

            if (i == 0) {
                /* need to begin path w/o end, or lines will not clear */
                ctx.beginPath();
                ctx.moveTo(x, y);
            } else {
                ctx.lineTo(x, y);
            }

            x += deltaX;
        }

        ctx.strokeStyle = "white";
        ctx.stroke();
    }
}
