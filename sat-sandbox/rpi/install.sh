#!/bin/bash

export HDIR=/var/www/html
export PATH_TO_LIBREPUS_DEV=/home/pi/librepus-dev

if [ "$(which lighttpd)" == "" ]; then
    # Install web-server `lighttpd` to rpi
    apt-get -y install lighttpd && \
    grep "$HDIR" /etc/lighttpd/lighttpd.conf && \
    mv $HDIR $HDIR.backup && \
    ln -s ${PATH_TO_LIBREPUS_DEV}/sat-sandbox/rpi/web-gauge $HDIR && \
    ls $HDIR && \
    sync
fi

# Recompile and restart `lpusnode` as a service
apt-get -y install libcmocka-dev && \
make clean all -C ${PATH_TO_LIBREPUS_DEV}/librepus && \
make clean all -C ${PATH_TO_LIBREPUS_DEV}/lpusnode && \
rm -rf /etc/systemd/system/lpusnode.service && \
ln -s ${PATH_TO_LIBREPUS_DEV}/sat-sandbox/rpi/lpusnode.service /etc/systemd/system/lpusnode.service && \
systemctl enable lpusnode.service && \
systemctl restart lpusnode.service

