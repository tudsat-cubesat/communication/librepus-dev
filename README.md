# LibrePUS-dev

This repository contains tools to support the development of the [LibrePUS](https://gitlab.com/tudsat-cubesat/communication/librepus-dev) library. 

## Features

- lpusnode: Transmits PUS messages between multiple instances of this utility via localhost UDP socket. Demonstrates encoding / decoding of all supported PUS services / subservices


## Build

It is necesarry to fetch the git submodules before building:


```
git submodule sync --recursive
git submodule update --init --recursive
```

Afterwards you can build `lpusnode`:

```
cd lpusnode
make all
```

## Contributing

Make sure to run `astyle` on all updated files before commiting, otherwise the CI pipeline will fail.

Only merge requests that pass the pipeline will be accepted.

## License

© 2021 [TU Darmstadt Space Technology e.V.](http://www.tudsat.space) and contributors

Licensed under the [GPLv3](./LICENSE).

